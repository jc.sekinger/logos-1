# Logos Ǧ1

## Description

### Sources vectorielles
logos Ǧ1 en svg, sur fond transparent, créés avec Inkscape (https://inkscape.org/)

### Format PNG
logos Ǧ1 en png, sur fond transparent, à utiliser directement dans vos fichiers.
## Auteur
Jean-Christophe Sekinger (jcs@aquilenet.fr)

## License
Licence Art Libre (https://artlibre.org/)
